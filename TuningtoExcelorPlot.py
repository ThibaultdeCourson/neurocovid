# -*- coding: utf-8 -*-
"""
Created on Fri Jun 10 13:20:59 2022

@author: niels

Script to create parallel plots or excel files of hyperparameter tuning runs
"""

import json
import os
import plotly.express as px
import pandas as pd
import plotly.io as pio
pio.renderers.default='browser'


picture=False #create parallel plot or not
excel=True #create results excel
from_excel=False #load data for plot or new excel from excel instead of keras trials
excel_path= "data/generated/run_AllGenes_1.xlsx" #path to save excel
from_excel_path= "data/generated/Grobtuning_top98.xlsx" # excel path to load from
rootdir = "tuner_1" #directory of trial files
#parallel_plot_path=r"C:\Users\niels\Documents\BA_PEM\Code\Tuning\parallel_plots\Grobtuning_top98.pdf" #paths for plots
#parallel_plot_path_1=r"C:\Users\niels\Documents\BA_PEM\Code\Tuning\parallel_plots\Feintuning_top78.pdf"
#parallel_plot_path_2=r"C:\Users\niels\Documents\BA_PEM\Code\Tuning\parallel_plots\Feinsttuning_2.pdf"
#load trial data

if not from_excel:
    vis_data=[]
    
    for subdirs, dirs, files in os.walk(rootdir):
        for file in files:
            if file.endswith("trial.json"):
              with open(subdirs + '/' + file, 'r') as json_file:
                data = json_file.read()
              vis_data.append(json.loads(data))
    
   #convert trials into dict and save in list
   #every hyperparameter that is not used needs to be commented out; every new one needs to be specified
    trials=[]
    for trial in vis_data:
    
        trial_data={'trial_number': trial, 
                    'learning_rate': trial['hyperparameters']['values']['learning_rate'],
                    'num_layers': trial['hyperparameters']['values']['num_layers'],
                    'activation': trial['hyperparameters']['values']['activation'],
                    #'loss': trial['metrics']['metrics']['loss']['observations'][0]['value'][0],
                    #'val_loss': trial['metrics']['metrics']['val_loss']['observations'][0]['value'][0],
                    'val_loss': trial['score'],
                    'stddev':trial['hyperparameters']['values']['stddev'],
                    'units_0':trial['hyperparameters']['values']['units_0'],
                    'droprate_0':trial['hyperparameters']['values']['droprate_0']
                    }
        
        for layer in range(1,trial_data['num_layers']):
            trial_data['units_'+str(layer)]=trial['hyperparameters']['values']['units_'+str(layer)]
            trial_data['droprate_'+str(layer)]=trial['hyperparameters']['values']['droprate_'+str(layer)]

        trials.append(trial_data)
    
    df=pd.DataFrame(trials)
else:
    df=pd.read_excel(from_excel_path)


#order in which to display hyperparameters in excel; hyperparameters need match with ones specified above    
order=["num_layers",
       "units_0","units_1","units_2","units_3","units_4",#"units_5",
      'droprate_0','droprate_1',
       'droprate_2','droprate_3','droprate_4',#'droprate_5',
       "learning_rate", "activation",
       'stddev',
      "val_loss"]
df=df.reindex(columns=order)
df=df.fillna(0)
#splits to create seperate plots if too many hyperparameters for one
# =============================================================================
# split_1=["num_layers",
#          "units_1","units_2","units_3","units_4","units_5",
#          'droprate_in',
#          'droprate_1',
#        'droprate_2','droprate_3','droprate_4','droprate_5', 
#        "val_loss"]
# split_2=["learning_rate", 'adam_1', 
#          'adam_2', 'epsilon_adam',
#          'batch_size','stddev',
# "mass_loss", "porosity_loss", 
# "val_loss"]
# 
# df_1=df[split_1]
# df_2=df[split_2]
# =============================================================================

#create plots; comment out plots if only one is to be created
if picture:
    fig = px.parallel_coordinates(df, labels={"learning_rate": "Lernrate",
                                            "batch_size": "Batch-Größe",
                                            'stddev': "Standardabweichung Rauschen",
                                            "droprate_1":"Dropoutrate Schicht 1",
                                            "droprate_in":"Dropoutrate Inputschicht",
                                            'weight_decay': 'L2-Norm Koeff.',
                                            "droprate_2":"Dropoutrate Schicht 2",
                                            "droprate_3":"Dropoutrate Schicht 3",
                                            "droprate_4":"Dropoutrate Schicht 4",
                                            "droprate_5":"Dropoutrate Schicht 5",
                                            "units_1": "Anzahl Einheiten Schicht 1",
                                            "units_2": "Anzahl Einheiten Schicht 2",
                                            "units_3": "Anzahl Einheiten Schicht 3",
                                            "units_4": "Anzahl Einheiten Schicht 4",
                                            "units_5": "Anzahl Einheiten Schicht 5",
                                            "num_layers": "Anzahl Schichten",
                                            'adam_1': "1. Adam Koeff.",
                                            'adam_2': "2. Adam Koeff.", 
                                            ##'epsilon_adam':"Adam Störterm",
                                            "mass_loss": "MSE Beladung", 
                                            "porosity_loss": "MSE Porosität", 
                                            "val_loss": "MSE gesamt"}, color='val_loss')
    fig.write_image(parallel_plot_path, width=1600, height=900)
    #fig.show()
    
    fig_1 = px.parallel_coordinates(df_1, labels={
                                            "droprate_in": "Dropoutrate Inputs",
                                            "droprate_1":"Dropoutrate Schicht 1",
                                            "droprate_2":"Dropoutrate Schicht 2",
                                            "droprate_3":"Dropoutrate Schicht 3",
                                            "droprate_4":"Dropoutrate Schicht 4",
                                            "droprate_5":"Dropoutrate Schicht 5",
                                            "units_1": "Einheiten Schicht 1",
                                            "units_2": "Einheiten Schicht 2",
                                            "units_3": "Einheiten Schicht 3",
                                            "units_4": "Einheiten Schicht 4",
                                            "units_5": "Einheiten Schicht 5",
                                            "num_layers": "Anzahl Schichten",
                                            "mass_loss": "durchschn. MSE Aktivmaterialmasse", 
                                            "porosity_loss": "durchschn. MSE Porosität", 
                                            "val_loss": "durchschn. MSE gesamt"}, color='val_loss'#, color_continuous_scale=['#00549F', '#C8C8C8']
        )
    fig_1.write_image(parallel_plot_path_1, width=1600, height=900)
    #fig_1.show()

    fig_2 = px.parallel_coordinates(df_2, labels={
                                'adam_1': "1. Adam Koeff.",
                                'adam_2': "2. Adam Koeff.", 
                                'epsilon_adam':"Adam Störterm",
                                "learning_rate": "Lernrate",
                                "batch_size": "Batch-Größe",
                                'stddev': "Standardabweichung Rauschen",
                                "mass_loss": "durchschn. MSE Aktivmaterialmasse", 
                                "porosity_loss": "durchschn. MSE Porosität", 
                                "val_loss": "durchschn. MSE gesamt"}, color='val_loss'#, color_continuous_scale=['#00549F', '#C8C8C8']
        )
    fig_2.write_image(parallel_plot_path_2, width=1600, height=900)
    #fig_2.show()
if excel:
    df.to_excel(excel_path, index=False)