# Neurocovid project: a Data Analysis approach

![](img/poster.jpg)

# Neurocovid project: a Deep Learning approach

Conducted by Thibault de Courson and Niels Hayen

# I - Introduction

## Project background

As we all experienced it these last couple of years, coronavirus 2 (SARS-CoV-2) is a very dangerous virus which leads to severe pulmonary and systemic disease in some individuals.
One of the most serious complications it can trigger is Acute Encephalopathy, a severe deterioration of the mental condition also called delirium.
Some proteins secreted by the human body as part of the immune inflammatory response could be the cause of this dysfunction.
The pathophysiology of infection-related encephalopathy is not well understood, and identifying gene signatures responsible is a first step towards a blood test for predicting and guiding treatments for encephalopathy.

## I.1 - Problem

Our objective is to study the connections between DNA expression and the development of encephalopathy in SARS-CoV-2 patients.
We have tried to model this relationship with some classical statistic models like logistic regression but they have not provided convincing results yet, leading us to believe that this relationship is too complex to be approximated by these models.
Artificial Neural Networks (ANN), however, are reputed to be able to reproduce complex models.
That is why we believe machine learning could provide an interesting perspective.
In this project, we try to build an ANN that can reproduce how the presence and the quantity of certain genes in the blood can impact the health condition of the patient.
This ANN could help us understand which genes are responsible for this complication but could also become a medical tool that could be used to estimate the risk of developing encephalopathy in the newly infected SARS-CoV-2 patients.

# II - Data cleaning

## II.1 - The data

To study how genetics influences the impact of the COVID-19 on the human body, we used data collected by the Hospital of the University of California between March and December 2020.
The cohort is constituted of 182 patients which had known or presumptive COVID-19.

544 peripheral blood samples were collected, using standard venous collection, on this cohort at various days.
A next-generation RNA sequencing of the monocytes of these samples was performed, offering a count of the number of occurrences of each gene found in the sample.
58,051 genes were accounted for, that is to say the essential of the human genome.

A second dataset provided us the information of the health state of the patient evalued by the medical personnel.
This data was collected for the COMET prospective study, which aims to describe the relationship between specific immunological assessments and the clinical courses of COVID-19 in hospitalized patients.
Among the symptoms identified by the medical personnel, the acute encephalopathy symptom was attributed to the patients which had a CAM-ICU scale score higher than 2.

## II.2 - Identifying the relevant genes

### II.2.1 - Filtering on gene frequencies

The difficulty occurring when trying to explain biological behaviours by the genome is the high complexity of the latter.
Using its entirety necessitate a number of features (one by gene, 58,051 in total) much higher than the number of samples (544, reduced to 235 when the dataset is balanced), which can be problematic for some models.
However, we believe that only few specific genes, called Differentially Expressed (DEG), are responsible for these biological mechanisms.
Thus, the modeling could be done using only these genes as features.
These genes have unfortunately not been identified, as the human genome is too complex to be entirely decoded by a human.
A challenge of the analysis is therefore to reduce the list of genes that could be differentially expressed.

A first preselection that can be easily done by removing the genes that are too rare: 3.677 genes do not appear at all in the samples studied and 41,346 appear less than 50 times in the whole dataset.
These genes are not in sufficient quality to make any impact on the patient so they can be discarded.
Thus, we can reduce the list of 58,051 genes to 13,028.

![](img/gene_frequencies.jpg)
Figure 1: Distribution of gene occurrences

### II.2.2 - Filtering using a Principal Component Analysis based Unsupervised Feature Extraction

To further reduce this list of potentially differentially expressed genes, we used the Principal Component Analysis based Unsupervised Feature Extraction (PCA-UFE), a method which consists in using a Principal Component Analysis (PCA) to create (and select, using the p-values of a t-test) a typical profile of a sick and a healthy patient, typical profiles whose values are then compared for each gene (using a Chi-Squared test and the BH procedure) to single out the genes whose quantity changes the most between the healthy and the sick patients.

![](img/PCAUFE_explanation_p1.png)
Figure 2: 1st step of the PCA-UFE: Filtering, balancing and transposing the dataset

![](img/PCAUFE_explanation_p2.png)
Figure 3: 2nd step: Performing a PCA on the transformed dataset and applying a T-test to the components generated to identify the two composite samples (in grey) that best differentiate the healthy patients (in green) from the sick ones (in red)

![](img/PCAUFE_explanation_p3.png)
Figure 4: 3rd step: Compute for each gene a p-value based on its difference in expression between the two composite patients and use that indicator to identify the Differentially Expressed Genes

The application of this analysis to our dataset provided a list of 40 to 350 differentially expressed genes, depending on the iteration.

![](img/PCAUFE.jpg)
Figure 5: Genes projected the composite samples generated by the PCA-UFE

### II.2.3 - Filtering using the Variance Inflation Factor

This list can be further reduced by eliminating the genes having a Variance Inflation Factor (VIF) higher than 10, which gives a final list of 15 to 40 DEG having a low linear collinearity.

## II.3 - Standardization

Before tuning and training the models the data was standardized to reduce the effect of different magnitudes of the feature inputs.
This was done by doing a z-transformation with sklearn's StandardScaler, which transforms the training data to a distribution with 0 mean and unit variance.
The target variable was furthermore transformed to a binary variable with 0 meaning no presence of Acute Encephalopathy and 1 meaning an occurrence of the sickness.

# III - Modeling

## III.1 - Using DEG identified by PCA-UFE

As the available data set is small compared to the amount of features in the complete data set, we first tuned and trained artificial neural networks using only the 38 genes identified by our last PCA-UFE iteration as input features.

### III.1.1 - Hyperparameter Tuning

In order to find the best possible hyperparameters, an exhaustive hyperparameter tuning was performed.
For this, we used a random search.
This has proven to be more efficient than grid search especially in settings of low effective dimensionality [BB12].

The search interval for the used hyperparameters is summarized in the following.

**Batch size**

Small batch sizes can reach a faster and more robust convergence, as new gradients are computed more frequently and are therefore more up-to-date.
They can also lead to better generalization [ML18].
A batch size of size $2^n$ is usually used to use processors with maximum computational efficiency.
Thus, the batch size is set to 4.
To limit the hyperparameter search space the batch size is not tuned.

**Training epochs and patience**

Early stopping was used to get the best epoch in model training with respect to the validation loss.
To ensure all models trained can reach there minimum loss within training time, models within the search space were first manually trained to see how long a period without improvement but a better loss in later training stages in the training can be.
No model exceeded 100 epochs without improvement and an improvement in validation loss after that.
The patience was then set to a conservative 200 epochs for the hyperparameter tuning.
The maximum number of epochs was set to 5000, to allow for further improvement should the early stopping not stop the training.

**Optimizer and learning rate**

Because of its widespread successful usage and good experiences with the optimizer in the course, the Adam optimizer is used in this project.
The parameters of the optimizer are set to the default settings except for the learning rate.
Adam incorporates an adaption of the learning rate and recommends a rate of 0.001 as default [KB14].
For this project, the learning rate is examined in an interval around this value from 0.0001 to 0.1.
The hyperparameter values are generated with a logarithmic distribution to find the right order of magnitude for the learning rate, which is assumed to lie more in the low spectrum of the interval.

**Number of layers and neurons**

To find out about a reasonable number of magnitude for the number of layers and neurons we tried to generate a model that is capable of overfitting the used data.
The overfitting is then aimed to be reduced by the use of overfitting measures such as dropout and noise augmentation.
A model with one layer and 16 neurons was able to overfit the data.
As a minimum number of neurons we chose 8.
This allows for passive regularization of the model by using a lower capacity then necessary for overfitting.
The maximum number of neurons was set to 64, the maximum number of layers to 5.
This allows for a more complex model that is needed with the use of dropout and noise augmentation.

**Activation function**

Two activation functions were successfully used in the course: The Rectified Linear Unit (ReLU) and the Exponential Linear Unit (ELU).
Thus, both activation function were allowed in the hyperparameter tuning.

**Regularization**

As explained in section 2.3, data augmentation was identified as a promising regularization measure for a small data set.
Thus, it is implemented in our tuning process.
This is achieved by adding a layer adding Gaussian noise to the data during the training process, which is then removed for the actual prediction.
This way, slightly different data points are used in each training epoch.
This leads to a synthetically enhanced data set and reduces overfitting.
For the tuning process a standard deviation for the Gaussian noise between 0 and 0.5 is allowed.

Another regularization method, which was introduced in the course and is considered 'state-of-the-art' is dropout.
This is applied for our model as well.
Dropout rates for each layer between 0 and 0.6 are allowed during the tuning process.

**Cross validation**

As the dataset we use offers a relatively limited number of samples (235), we used 5-fold cross-validation.
Our model is thus fitted five times on different 4/5 of the data, being evaluated each time on the last 1/5.
The average of these five validation losses is then used as the validation loss for the model.
This allows the usage of all training data points to generate a more accurate estimate of the model performance during the tuning process.

**Tuning results**

A total of 215 different models were evaluated using the explained hyperparameters.
Of these, the best 10 are summarized in Table 1 below.

|                   | **1** | **2** | **3** | **4** | **5** | **6** | **7** | **8** | **9** | **10** |
|-------------------|-------|-------|-------|-------|-------|-------|-------|-------|-------|--------|
| **n layers**      | 1     | 2     | 3     | 2     | 4     | 1     | 4     | 3     | 1     | 3      |
| **units 1**       | 20    | 12    | 19    | 40    | 52    | 37    | 33    | 54    | 25    | 11     |
| **units 2**       |       | 36    | 37    | 44    | 24    |       | 49    | 13    |       | 64     |
| **units 3**       |       |       | 59    |       | 32    |       | 39    | 46    |       | 44     |
| **units 4**       |       |       |       |       | 8     |       | 56    |       |       |        |
| **units 5**       |       |       |       |       |       |       |       |       |       |        |
| **drop rate 1**   | 0.25  | 0.07  | 0.46  | 0.31  | 0.13  | 0.43  | 0.52  | 0.25  | 0.36  | 0.34   |
| **drop rate 2**   |       | 0.41  | 0.35  | 0.44  | 0.59  |       | 0.35  | 0.07  |       | 0.39   |
| **drop rate 3**   |       |       | 0.27  |       | 0.32  |       | 0.06  | 0.18  |       | 0.18   |
| **drop rate 4**   |       |       |       |       |       |       | 0.39  |       |       |        |
| **drop rate 5**   |       |       |       |       |       |       |       |       |       |        |
| **learning rate** | 0.054 | 0.006 | 0.010 | 0.010 | 0.008 | 0.036 | 0.002 | 0.013 | 0.018 | 0.011  |
| **activation**    | elu   | relu  | relu  | relu  | relu  | relu  | elu   | elu   | elu   | elu    |
| **std dev**       | 0.50  | 0.11  | 0.04  | 0.22  | 0.41  | 0.41  | 0.09  | 0.48  | 0.48  | 0.49   |
| **val loss**      | 0.56  | 0.58  | 0.58  | 0.58  | 0.58  | 0.58  | 0.59  | 0.59  | 0.59  | 0.59   |

Table 1: 10 best models obtained from hyperparameter tuning with PCA-UFE filtered genes

Several observations can be made.
First the model seems to work better with a small number of layers, the best model using 5 layers being the 32th.
The Dropout layers seems also to be useful, as all of the 10 best models use the with a rate of at least 25%.
However, the other hyperparameters does not show any clear trend: the number of layers oscillate from 1 to 4, the number of units reaches both extremes in the 10 best models.

As especially the two independent regularization methods of dropout and noise augmentation affect each other and thus make an analysis difficult, we then tried another tuning run with 25 iterations using only one of the techniques separately.
Except for that and omitting 5 layered networks, we used the same configuration as before.
The results for the sole use of noise augmentation without dropout are summarized in Table 2 below.
Those without noise augmentation are shown in Table 3.

   
|                   | **1** | **2** | **3** | **4** | **5** | **6** | **7** | **8** | **9** | **10** |
|-------------------|-------|-------|-------|-------|-------|-------|-------|-------|-------|--------|
| **n layers**      | 3     | 4     | 3     | 2     | 2     | 2     | 3     | 1     | 3     | 1      |
| **units 1**       | 57    | 38    | 25    | 55    | 8     | 16    | 12    | 43    | 34    | 20     |
| **units 2**       | 11    | 51    | 8     | 23    | 28    | 40    | 25    |       | 20    |        |
| **units 3**       | 38    | 53    | 8     |       |       |       | 34    |       | 26    |        |
| **units 4**       |       | 8     |       |       |       |       |       |       |       |        |
| **learning rate** | 0.032 | 0.011 | 0.008 | 0.011 | 0.001 | 0.035 | 0.014 | 0.002 | 0.015 | 0.007  |
| **activation**    | relu  | relu  | relu  | relu  | relu  | elu   | relu  | relu  | relu  | elu    |
| **std dev**       | 0.31  | 0.47  | 0.23  | 0.48  | 0.32  | 0.30  | 0.47  | 0.23  | 0.08  | 0.27   |
| **val loss**      | 0.59  | 0.59  | 0.61  | 0.61  | 0.62  | 0.62  | 0.63  | 0.63  | 0.63  | 0.64   |

Table 2: 10 best models obtained from hyperparameter tuning with PCA-UFE filtered genes without dropout

It can be concluded from these results that networks with no dropout perform better with a higher rate of noise (a higher standard deviation).
Thus, it seems to be an effective technique.
Without dropout however, no network achieves a better result than in the initial tuning run.
These networks were therefore no longer analyzed.

Regarding models without noise, we also see that they do not outperform the initial tuning run.
We can conclude that noise augmentation helps decreasing the validation loss and should therefore also be kept in our final model.

|                   | **1** | **2** | **3** | **4**  | **5** | **6** | **7**  | **8**  | **9** | **10** |
|-------------------|-------|-------|-------|--------|-------|-------|--------|--------|-------|--------|
| **n layers**      | 4     | 3     | 5     | 2      | 4     | 2     | 2      | 4      | 3     | 4      |
| **units 1**       | 45    | 31    | 22    | 38     | 46    | 24    | 53     | 9      | 43    | 42     |
| **units 2**       | 43    | 54    | 58    | 64     | 8     | 24    | 44     | 16     | 23    | 43     |
| **units 3**       | 56    | 41    | 23    |        | 8     |       |        | 19     | 49    | 45     |
| **units 4**       | 45    |       | 24    |        | 8     |       |        | 23     |       | 60     |
| **units 5**       |       |       | 23    |        |       |       |        |        |       |        |
| **drop rate 1**   | 0.01  | 0.40  | 0.49  | 0.54   | 0.37  | 0.01  | 0.10   | 0.47   | 0.08  | 0.28   |
| **drop rate 2**   | 0.51  | 0.26  | 0.18  | 0.35   |       | 0.31  | 0.55   | 0.13   | 0.41  | 0.19   |
| **drop rate 3**   | 0.02  | 0.58  | 0.41  |        |       |       |        | 0.42   | 0.58  | 0.37   |
| **drop rate 4**   | 0.26  | 0.32  |       |        |       |       | 0.45   |        | 0.13  |        |
 | **drop rate 5**   |       |       | 0.20  |        |       |       |        |        |       |        |
| **learning rate** | 0.005 | 0.002 | 0.005 | 0.0006 | 0.043 | 0.011 | 0.0003 | 0.0003 | 0.001 | 0.004  |
| **activation**    | relu  | elu   | relu  | elu    | elu   | relu  | relu   | relu   | elu   | elu    |
| **val loss**      | 0.61  | 0.62  | 0.62  | 0.63   | 0.63  | 0.63  | 0.63   | 0.64   | 0.65  | 0.65   |

Table 3: 10 best models obtained from hyperparameter tuning with PCA-UFE filtered genes without noise augmentation

### III.1.2 - Training and validation

To retrieve the best model possible, the three best hyperparameter settings were then used to train final models.
These were each trained three times to reduce the effect of the random initialization.
To be able to use early stopping and retrieve the exact model at that step, no cross validation was used.
Instead, we split the training set into a validation set containing 15 % of the previous training data and a training set with the remaining data points.
The results of these trainings are summarized in Table 4 below.
The number of the models refers to the hyperparameter settings in Table 1.

 |                            | **model 1** | **model 2** | **model 3** |
|----------------------------|-------------|-------------|-------------|
| **1. validation loss**     | 0.455       | 0.630       | 0.631       |
| **1. validation accuracy** | 0.625       | 0.679       | 0.679       |
| **2. validation loss**     | 0.519       | 0.670       | 0.651       |
| **2. validation accuracy** | 0.625       | 0.714       | 0.607       |
| **3. validation loss**     | 0.423       | 0.667       | 0.605       |
| **3. validation accuracy** | 0.750       | 0.500       | 0.679       |

Table 4: Validation results for the three best models trained with PCA-UFE filtered genes

It can be seen that while the first model generally performed better than during the tuning, models 2 and 3 performed worse.
Thus, the model the most appropriated to this problem, with a validation loss of 0.42 and a validation accuracy of 75%, is the 3rd iteration of model 1.
This model is composed of 4 layers, with respectively 45, 43, 56 and 45 units, of 4 dropout layers with drop rates of 1%, 51%, 2% and 26%, a ReLU activation function, and a learning rate of 0.005.

### III.1.3 - Evaluation

Applying the chosen model to our test set, we got a loss of 4.763 and an accuracy of 67%.
The model offers thus performances too uneven to be satisfactory.

## III.2 - Using all genes

We decided to try also to train our models on all 13,028 genes preselected based on frequency to see if a neural network was able to handle a number of features higher than the number of samples.

For that, we re-used the hyperparameter tuning parameters described in 3.1.1 as they were adapted to the data.

A total of 92 different models were evaluated.
Of these, the best 10 are summarized in table below.

|                   | **1**  | **2**  | **3**  | **4**  | **5**  | **6**  | **7**  | **8**  | **9**  | **10** |
|-------------------|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|
| **n layers**      | 4      | 4      | 2      | 4      | 1      | 3      | 3      | 2      | 2      | 2      |
| **units 1**       | 28     | 36     | 10     | 36     | 35     | 61     | 32     | 10     | 38     | 19     |
| **units 2**       | 42     | 8      | 41     | 60     |        | 42     | 31     | 6      | 39     | 56     |
| **units 3**       | 28     | 8      |        | 19     |        | 19     | 23     |        |        |        |
| **units 4**       | 47     | 8      |        | 13     |        |        |        |        |        |        |
| **drop rate 1**   | 0.21   | 0.48   | 0.29   | 0.25   | 0.15   | 0.57   | 0.36   | 0.37   | 0.52   | 0.52   |
| **drop rate 2**   | 0.50   |        | 0.29   | 0.44   |        | 0.25   | 0.11   | 0.55   | 0.01   | 0.43   |
| **drop rate 3**   | 0.57   |        |        | 0.45   |        | 0.35   | 0.31   |        |        |        |
| **drop rate 4**   | 0.11   |        |        | 0.02   |        |        |        |        |        |        |
| **learning rate** | 0.0001 | 0.0019 | 0.0113 | 0.0001 | 0.0001 | 0.0002 | 0.0005 | 0.0010 | 0.0089 | 0.0075 |
| **activation**    | relu   | relu   | relu   | relu   | elu    | relu   | relu   | relu   | relu   | relu   |
| **std dev**       | 0.010  | 0.323  | 0.036  | 0.124  | 0.364  | 0.098  | 0.226  | 0.177  | 0.068  | 0.240  |
| **val loss**      | 0.479  | 0.485  | 0.491  | 0.500  | 0.509  | 0.510  | 0.516  | 0.524  | 0.528  | 0.545  |

Table 5: 10 best models obtained from hyperparameter tuning with genes filtered only on frequency

Surprisingly, our hyperparameter tuning process gave us better results with this high number of features.
However, there was again no clear trend concerning the examined hyperparameters.
Because of the positive influence of noise and dropout in the previous search, the runs without this measures were not repeated on this data set.

### III.2.1 - Training and validation

As done for the PCAUFE models, we retrieved the best three models of the tuning run and trained them three times, once again with different random initialization and without cross validation.

The results of this training and validation for all genes are summarized in the table below.
The number of the models refers to the hyperparameter settings in Table 1.

|                            | **model 1** | **model 2** | **model 3** |
|----------------------------|-------------|-------------|-------------|
| **1. validation loss**     | 0.544       | 0.433       | 0.508       |
| **1. validation accuracy** | 0.708       | 0.792       | 0.75        |             
| **2. validation loss**     | 0.237       | 0.521       | 0.462       |
| **2. validation accuracy** | 0.917       | 0.583       | 0.792       |             
| **3. validation loss**     | 0.538       | 0.404       | 0.617       |
| **3. validation accuracy** | 0.708       | 0.833       | 0.417       |             

Table 6: Validation results for the three best models trained with all genes

As shown on the previous table, the 2nd iteration of the best model yields the best validation accuracy (92%) and loss (0.24).
It is thus the most appropriated to this problem.
The chosen model is composed of 4 layers, with respectively 28, 42, 28 and 47 units, of 4 dropout layers with drop rates of 21%, 50%, 57% and 11%, a ReLU activation function, and a learning rate of 0.005.

### III.2.2 - Evaluation

Applying the chosen model to our test set, we got a loss of 0.93 and an accuracy of 65%.
the model offers thus performances too uneven to be satisfactory.
It becomes also less accurate than the model using PCA-UFE selected genes.

# IV - Conclusion

In this section we conclude our results and offer explanations for the achieved performance.
We also propose future steps to improve the classification performance.

## IV.1 - Our results

Unfortunately, the trained neural networks did not achieve satisfactory performances, as the best model obtained offers an accuracy of only 67% on test data.

The model using all pre-selected genes as features performs significantly better during hyperparameter tuning than the model using PCA-UFE reduced features.
On the test data the PCA-UFE based model reaches however a significantly lower loss and a higher accuracy.
Thus, we can conclude that either a lot of genes are responsible for the occurrence of acute encephalopathy or the DEG identification process does not work effectively.

However, the efficiency of the machine learning as mainly be limited by the relatively small quantity of samples.
The data set does not seem to be sufficient to fit a neural network with a high enough prediction accuracy.
More data would therefore be a major contribution towards improved performance.
This data is, however, very hard to generate in a medical setting with human patients.
Thus, other approaches, explained in in the future steps below, could be tried to improve ours results.

## IV.2 - Future steps

We believe that a more thorough hyperparameter tuning process could lead to better results.
We had in this analysis arbitrary fixed some hyperparameters that could be tuned in the future, in particular the batch size and the type of optimizer.
While keeping the Adam optimizer, its parameters could also be explored.
We could also try to change the preprocessing of the data.
We used standardization but we could also use a min-max scaler, divide the values by the median or not normalize the data at all.
Moreover, our analysis suffers from a lack of trials, as it is highlighted by the impossibility to identify a clear trend among the best results obtained from the search.
Augmenting the number of trials could allow to identify with greater certainty the best ANN structures but would also require more time as hyperparameter tuning with cross-validation is a slow process.
We could also try to pretrain the model using an AutoEncoder in order to have better initial values.

Ultimately the factor determining the possibility of building an accurate model to simulate these biological mechanisms is the actual number of Differentially Expressed Genes.
If it is low enough the current number of samples could be enough to train a model.
Otherwise, more data would be necessary.

If the number of DEG is, indeed, low, we would require a better identification technique in order to train our models with a smaller and more accurate set of features.
An identification technique that could be tried would be a differential gene expression analysis based on the negative binomial distribution (using the software DESeq2 for example).
We could indeed interpret its results by generating a volcano plot, which allows to visualize the direction, magnitude, and significance of changes in gene expression.
We could also try to train our model using the DEG identified by the VIF.

Finally, a option that could be tried to circumvent the problem of the high quantity of features would be to use Convolutional Neural Networks (CNN).
Indeed these models are specially adapted to datasets containing more features than data points.
To do so we could convert each data point to a heatmap by converting the number of occurrences of each gene to a shade of gray for a pixel.
We could then use these images to train the CNN.

# References
[BB12] James Bergstra and Yoshua Bengio. “Random search for hyper-parameter optimization”.
In: *J. Mach. Learn. Res.* 13.2 (2012). issn: 1532-4435.

[KB14] Diederik P. Kingma and Jimmy Ba. *Adam: A Method for Stochastic Optimization.*
Published as a conference paper at the 3rd International Conference for Learning Representations, San Diego, 2015. 2014.
url: http://arxiv.org/pdf/1412.6980v9.

[ML18] Dominic Masters and Carlo Luschi. *Revisiting Small Batch Training for Deep Neural Networks.* 2018.
url: http://arxiv.org/pdf/1804.07612v1.